<?php 
    const IMG = 'assets/img/';
?>

<!DOCTYPE html>
<html class="no-js" lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    
    <!-- Check if JS is enabled, remove html "no-js" class    -->
    <script>(function (html) {
        html.className = html.className.replace(/\bno-js\b/, 'js')
      })(document.documentElement);</script>

    <?php
        // Add dynamic body body class based on current script name
        $body_class = null;
        if ( isset( $_SERVER['SCRIPT_NAME'] ) ) {
            $script_name = explode( '/', $_SERVER['SCRIPT_NAME'] );
            $filename    = $script_name[ count( $script_name ) - 1 ];
            $filename    = explode( '.', $filename )[0];
            $body_class  = $filename;
        }
    ?>

    <!-- external styles here -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&family=Jura:wght@300;500;700&family=Maven+Pro:wght@400;600;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <!-- /external styles here -->
    <link rel="stylesheet" type="text/css" href="fullpage.css" />
     <link
    rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>

    <?php
        if ( file_exists( __DIR__ . '/assets/css/style.min.css' ) ) {
            echo '<link rel="stylesheet" href="assets/css/style.min.css">';
        }
        if ( file_exists( __DIR__ . '/assets/css/style.css' ) ) {
            echo '<link rel="stylesheet" href="assets/css/style.css">';
        }
    ?>
    
    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous">        
    </script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="vendors/easings.min.js"></script>
    <script type="text/javascript" src="vendors/scrolloverflow.min.js"></script>
    <script type="text/javascript" src="fullpage.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

    <?php
        if ( file_exists( __DIR__ . '/assets/js/main.min.js' ) ) {
            echo '<script src="assets/js/main.min.js" defer></script>';
        }
        if ( file_exists( __DIR__ . '/assets/js/main.js' ) ) {
            echo '<script src="assets/js/main.js" defer></script>';
        }
    ?>


</head>
<body class="<?= $body_class ?> body-main">
<!--[if lt IE 11]>
<p>Używasz bardzo starej wersji przeglądarki Internet Explorer. Z&nbsp;tego też powodu ta&nbsp;strona najprawdopodobniej nie&nbsp;działa prawidłowo oraz narażasz się na potencjalne ryzyko związane z bezpieczeństwem.
  <a href="https://www.microsoft.com/pl-pl/download/Internet-Explorer-11-for-Windows-7-details.aspx" rel="noopener noreferrer">Wejdź tutaj, aby ją zaktualizować.</a>
</p>
<![endif]-->
<main>

<div id="preloader">
<div class="wrapper">
  <div class="triangle-wrap">
      <div class="triangle triangle--main"></div>
      <div class="triangle triangle--red"></div>
      <div class="triangle triangle--blue"></div>
    <div class="triangle__text">Loading</div>
  </div>
</div>
</div>

<div class="icon-bar-one">
    <a href="https://gitlab.com/Gwiazdowicz" class="lab"><i class="fa fa-gitlab"></i></a> 
    <a href="https://www.linkedin.com/in/adam-gwiazdowicz-749656194/" class="linkedin"><i class="fa fa-linkedin"></i></a>
    <a href="tel:+48-734-468-794" class="phone"><i class="fa fa-phone"></i></a> 
    <a href="mailto:gwiazdowiczadam@gmail.com" class="google"><i class="fa fa-google"></i></a>
</div>

<div id="fullpage">
<div class="section" id="section0">   
<div id="top" class="header ">
    <div class="header__container">
        <div class="header__boarder">
            <div class="header__tile">
                <div class="header__logo">
                   <a class="header__logo-title" href="#!">AG</a>
                </div>

                <div class="icon">
                    <div class="hamburger"></div> 
                </div>
                <ul  id="menua" class="header__menu">
                    <li  data-menuanchor="skil-m" class="header__title"><a href="#skil-m">Skils</a></li>
                    <li  data-menuanchor="exp-m" class="header__title"><a href="#exp-m">Experience</a></li>
                    <li  data-menuanchor="pro-m" class="header__title"><a href="#pro-m">Projects</a></li>
                    <li  data-menuanchor="cont-m" class="header__title"><a href="#cont-m">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>












