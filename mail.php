
<?php 
const IMG = 'assets/img/';
?>

<!DOCTYPE html>
<html class="no-js" lang="pl">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title></title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">


<!-- Check if JS is enabled, remove html "no-js" class    -->
<script>(function (html) {
    html.className = html.className.replace(/\bno-js\b/, 'js')
  })(document.documentElement);</script>

<?php
    // Add dynamic body body class based on current script name
    $body_class = null;
    if ( isset( $_SERVER['SCRIPT_NAME'] ) ) {
        $script_name = explode( '/', $_SERVER['SCRIPT_NAME'] );
        $filename    = $script_name[ count( $script_name ) - 1 ];
        $filename    = explode( '.', $filename )[0];
        $body_class  = $filename;
    }
?>

<!-- external styles here -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&family=Jura:wght@300;500;700&family=Maven+Pro:wght@400;600;800&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
<!-- /external styles here -->

<?php
    if ( file_exists( __DIR__ . '/assets/css/style.min.css' ) ) {
        echo '<link rel="stylesheet" href="assets/css/style.min.css">';
    }
    if ( file_exists( __DIR__ . '/assets/css/style.css' ) ) {
        echo '<link rel="stylesheet" href="assets/css/style.css">';
    }
?>

<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous">        
</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<?php
    if ( file_exists( __DIR__ . '/assets/js/main.min.js' ) ) {
        echo '<script src="assets/js/main.min.js" defer></script>';
    }
    if ( file_exists( __DIR__ . '/assets/js/main.js' ) ) {
        echo '<script src="assets/js/main.js" defer></script>';
    }
?>

</head>
<body class="<?= $body_class ?>">
<!--[if lt IE 11]>
<p>Używasz bardzo starej wersji przeglądarki Internet Explorer. Z&nbsp;tego też powodu ta&nbsp;strona najprawdopodobniej nie&nbsp;działa prawidłowo oraz narażasz się na potencjalne ryzyko związane z bezpieczeństwem.
<a href="https://www.microsoft.com/pl-pl/download/Internet-Explorer-11-for-Windows-7-details.aspx" rel="noopener noreferrer">Wejdź tutaj, aby ją zaktualizować.</a>
</p>
<![endif]-->
<main>


<div id="top" class="header">
<div class="header__container">
    <div class="header__boarder">
        <div class="header__tile">
            <div class="header__logo">
               <a class="header__logo-title" href="/ag">AG</a>
            </div>
        </div>
    </div>
</div>
</div>

<?php

$to      = 'gwiazdowiczadam@gmail.com';
$name    = $_POST['name'];
$email   = $_POST['email'];
$subject = 'Nowy e-mail od ' . $name . ' (' . $email . ')';
$message = $_POST['message'];
$headers = 'From: ' . $name . ' (' . $email . ')';
$headers .= 'Content-Type: text/html; charset=utf-8';

mail($to, $subject, $message, $headers);


?>


<div class="hero">
    <div class="hero__container">
        <div class="hero__border">
            <div class="hero__box-mail">
                <div class="hero__star"></div>
                <?php echo ' <h1 class="hero__title a-title">Message Send</h1>'; ?>
                <h2 class="hero__subtitle a-subtitle"></h2>
                <h2 class="hero__subtitle--info">Thank you very much <br> I will contact with you soon</h2>
                <div class="hero__star hero__star--two"></div>
                <div class="hero__star hero__star--three"></div>
                <div class="hero__btn">
                    <a href="http://localhost:3000/ag/">Main Page</a>
                </div>
                <div class="hero__bottom-box">
                    <p>Adam Gwiazdowicz</p>
                    <a href="tel:+48734468794">+48 734 468 794</a>
                    <a href="mailto:gwiazdowiczadam@gmail.com">gwiazdowiczadam@gmail.com</a>
                </div>
            </div>
        </div>
    </div>
</div>


<footer class="footer">
    <div class="footer__container">
        <div class="footer__border">
            <div class="footer__box">
                <div class="footer__col">
                    <ul>
                        <a href="/ag"><li>Main Page</li></a>
                        <a href="#!"><li>Skils</li></a>
                        <a href="#!"><li>Experience</li></a>
                        <a href="#!"><li>About</li></a>
                        <a href="#!"><li>Contact</li></a>
                    </ul>
                </div>
                <div class="footer__col-two">
                    <div class="footer__row">
                        <div class="footer__logo">
                            <a href=
                            >AG</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="#!"><div class="footer__arrows"></div></a>
    <div class="footer__footer">
        <p>2020 © Made with passion by AG</p>
    </div>
</footer>