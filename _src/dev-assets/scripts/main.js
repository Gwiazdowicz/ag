import slick from 'slick-carousel';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import fullpage from 'fullpage.js';

console.log('main.js imported');


if($("body").hasClass('body-main')){
  $('#preloader').delay(2000).fadeOut('slow'); 
  var distanceOne = $('#section2').offset().top;
  var distanceTwo = $('#section3').offset().top,
  $window = $(window);
}

($("#about").hasClass("opened"))

  var myFullpage = new fullpage('#fullpage', {
    responsiveWidth: 1200,
    anchors: ['TopHero','about-m','exp-m', 'skil-m', 'pro-m', 'cont-m'],
    menu: '#menu1',
    slidesNavigation: true,
    //scrollBar:true,
  });

  var scrollable = $('.fp-section').find('.fp-scrollable'); 
  var iScrollInstance = scrollable .data('iscrollInstance');

$(document).ready(function(){

    $(".icon").click(function(){
       $(".icon").toggleClass("active");
       $(".header__menu").toggleClass("off");
     })
   })

   $(document).ready(function(){
    $(".icon").click(function(){
       $(".icon").toggleClass("active");
       $("aside").toggleClass("off");
     })
   })



   $('.slick').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: true,
    dots: true,
    fade: false,
    arrows:false,
    focusOnSelect: true,
    autoplay: true,
    speed:1000,
    mobileFirst: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 3
        }
      },

      {
        breakpoint: 700,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },

      {
        breakpoint: 500,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false,
        }
      },

    ]
});

  $('.slick2').slick({
    nextArrow: document.getElementById('arrow-left'),
    prevArrow: document.getElementById('arrow-right'),
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows:true,
    fade:true,
    swipe:false,
  });

  $('.slick-r').slick({
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplaySpeed: 4000,
    delay: 5000,
    arrows:false,
    dots:true,
    speed: 700,
    mobileFirst: true,
    responsive: [

      {
        breakpoint: 1200,
        settings: "unslick"
    },
    {
      breakpoint: 1000,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },

    {
      breakpoint: 650,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    ]
});


$('#btn-1, #btn-2, #btn-3, #btn-4').click(function(){  
 var id = (this.id);
 var turn = $(this).attr("turn");
 console.log(turn);

 if(id=="btn-1" && turn=="false"){ 
  $('.experience__text').slideUp('slow');
  $('#box-1').slideToggle('slow');
  $('#btn-1').attr("turn", "true");
  $('#btn-2').attr("turn", "false");
  $('#btn-3').attr("turn", "false");
  $('#btn-4').attr("turn", "false");
  $("#btn-1").html("Close");
  $("#btn-2").html("Expand it");
  $("#btn-3").html("Expand it");
  $("#btn-4").html("Expand it");
  if ($(window).width() < 900) {
    $(this).attr("href", "#more-tile"); 
  }
 }

 else if(id=="btn-1" && turn=="true"){
  $('.experience__text').slideUp('slow');
  $('#box-1').slideUp('slow');
  $('#btn-1').attr("turn", "false");
  $('#btn-2').attr("turn", "false");
  $('#btn-3').attr("turn", "false");
  $('#btn-4').attr("turn", "false");
  $("#btn-1").html("Expand it");
 }

 else if(id=="btn-2" && turn=="false"){
  $('.experience__text').slideUp('slow');
  $('#box-2').slideToggle('slow');
  $('#btn-2').attr("turn", "true");
  $('#btn-1').attr("turn", "false");
  $('#btn-3').attr("turn", "false");
  $('#btn-4').attr("turn", "false");
  $("#btn-2").html("Close");
  $("#btn-1").html("Expand it");
  $("#btn-3").html("Expand it");
  $("#btn-4").html("Expand it");
  if ($(window).width() < 900) {
    $(this).attr("href", "#more-tile"); 
  }
 }

 else if(id=="btn-2" && turn=="true"){
  $('.experience__text').slideUp('slow');
  $('#box-2').slideUp('slow');
  $('#btn-2').attr("turn", "false");
  $("#btn-2").html("Expand it");
 }

 else if(id=="btn-3" && turn=="false"){
  $('.experience__text').slideUp('slow');
  $('#box-3').slideToggle('slow');
  $('#btn-3').attr("turn", "true");
  $('#btn-1').attr("turn", "false");
  $('#btn-2').attr("turn", "false");
  $('#btn-4').attr("turn", "false");
  $("#btn-3").html("Close");
  $("#btn-1").html("Expand it");
  $("#btn-2").html("Expand it");
  $("#btn-4").html("Expand it");
  if ($(window).width() < 900) {
    $(this).attr("href", "#more-tile"); 
  }
 }

 else if(id=="btn-3" && turn=="true"){
  $('.experience__text').slideUp('slow');
  $('#box-3').slideUp('slow');
  $('#btn-3').attr("turn", "false");
  $("#btn-3").html("Expand it");
 }

 else if(id=="btn-4" && turn=="false"){
  $('.experience__text').slideUp('slow');
  $('#box-4').slideToggle('slow');
  $('#btn-4').attr("turn", "true");
  $('#btn-1').attr("turn", "false");
  $('#btn-2').attr("turn", "false");
  $('#btn-3').attr("turn", "false");
  $("#btn-4").html("Close");
  $("#btn-1").html("Expand it");
  $("#btn-2").html("Expand it");
  $("#btn-3").html("Expand it");
  if ($(window).width() < 900) {
    $(this).attr("href", "#more-tile"); 
  }
 }

 else if(id=="btn-4" && turn=="true"){
  $('.experience__text').slideUp('slow');
  $('#box-4').slideUp('slow');
  $('#btn-4').attr("turn", "false");
  $("#btn-4").html("Expand it");
 }
});



jQuery(function($) {
  
  // Function which adds the 'animated' class to any '.animatable' in view
  var doAnimations = function() {
    
    // Calc current offset and get all animatables
    var offset = $(window).scrollTop() + $(window).height(),
        $animatables = $('.animatable');
    
    // Check all animatables and animate them if necessary
		$animatables.each(function(i) {
       var $animatable = $(this);
      
      // Items that are "above the fold"
			if (($animatable.offset().top + $animatable.height() + 50) < offset) {
        
        // Item previously wasn't marked as "above the fold": mark it now
        if (!$animatable.hasClass('animate-in')) {
          $animatable.removeClass('animate-out').css('top', $animatable.css('top')).addClass('animate-in');
        }

			}
      
      // Items that are "below the fold"
      else if (($animatable.offset().top + $animatable.height() + 50) > offset) {
        
        // Item previously wasn't marked as "below the fold": mark it now
        if ($animatable.hasClass('animate-in')) {
          $animatable.removeClass('animate-in').css('top', $animatable.css('top')).addClass('animate-out');
        }

      }

    });

	};
  
  // Hook doAnimations on scroll, and trigger a scroll
	$(window).on('scroll', doAnimations);
  $(window).trigger('scroll');

});



















  
