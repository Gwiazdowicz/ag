<div class="section" id="section1">
<div id="about" class="about">
    <div class="about__container">
        <div class="about__border">
            <div class="about__box">
                <div class="about__head">
                    <div class="a-paddle"></div>
                    <h2 class="about__title a-subtitle a-subtitle--white">About</h2>
                    <div class="a-paddle"></div>
                    <p class="about__number a-number a-number--white">02</p>
                </div>
                <div class="about__tile">
                    <div class="about__img"></div>
                    <div class="about__text">
                        <h3>About me</h3>
                        <p class="a-article a-article--white">My name is Adam Gwiazdowicz and I am a front-end developer. I am a computer science graduate specializing in mobile applications. But I've spent the last years developing web development skills. I have two great passions in programming and sport. which complement each other. I invite you to familiarize yourself with my projects.</p></div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
     
