<div class="section" id="section2">
<div id="experience" class="experience">
    <div class="experience__container">
        <div class="experience__border">
            <div class="skils__head">
                <div class="a-paddle"></div>
                <h2 class="skils__title a-subtitle a-subtitle--white">experience</h2>
                <div class="a-paddle"></div>
                <p class="skils__number a-number a-number--white">03</p>
            </div>

            <div id="box-experience" class="experience__box">
                <div class="experience__tile">
                    <div class="experience__img experience__img--one"></div>
                    <div class="experience__text-box">
                        <h3 class="experience__title">Develop</h3>
                        <a id="btn-1" turn="false" class="experience__button" href="#!">Expand it</a>
                    </div>
                </div>

                <div class="experience__tile">
                    <div class="experience__img experience__img--two"></div>
                    <div class="experience__text-box">
                        <h3 class="experience__title">Graphic</h3>
                        <a id="btn-2" turn="false" class="experience__button" href="#!">Expand it</a>
                    </div>
                </div>

                <div class="experience__tile">
                    <div class="experience__img experience__img--three"></div>
                    <div class="experience__text-box">
                        <h3 class="experience__title">Public speaking</h3>
                        <a id="btn-3" turn="false" class="experience__button"  href="#!">Expand it</a>
                    </div>
                </div>

                <div id="more-tile" class="experience__tile">
                    <div class="experience__img experience__img--four"></div>
                    <div class="experience__text-box">
                        <h3 class="experience__title">Sport</h3>
                        <a id="btn-4" turn="false" class="experience__button"  href="#!">Expand it</a>
                    </div>
                </div>
        
            </div>

            <div id="more" class="experience__more">
                <div id="box-1" class="experience__text">
                    <p class="experience__article">Front-end developer</p>
                    <p class="experience__article-discription">7 months of commercial experience</p>
                </div>
               

                <div id="box-2" class="experience__text">
                    <p class="experience__article">Advertising activities</p>
                    <p class="experience__article-discription">1,5 year of commercial experience</p>
                    <p class="experience__article">drone operator</p>
                    <p class="experience__article-discription">1,5 year of commercial experience</p>
                    <p class="experience__article">photographer</p>
                    <p class="experience__article-discription">Few episodes</p>
                </div>


                <div id="box-3" class="experience__text">
                    <p class="experience__article">announcer</p>
                    <p class="experience__article-discription">2 years of commercial experience</p>
                </div>

                <div id="box-4" class="experience__text">
                    <p class="experience__article">Football coach</p>
                    <p class="experience__article-discription">2 years of commercial experience</p>

                    <p class="experience__article">Swimming instructor</p>
                    <p class="experience__article-discription">4 years of commercial experience</p>

                    <p class="experience__article">Skiing instructor</p>
                    <p class="experience__article-discription">3 years of commercial experience</p>

                    <p class="experience__article">Aqua Fitness instruktor</p>
                    <p class="experience__article-discription">2 years of commercial experience</p>
                </div>
            </div>

            
        </div>
    </div>
</div>
</div>