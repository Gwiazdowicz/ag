<div class="section" id="section5">
<div id="contactw" class="contact section">
    <div class="contact__container">
        <div class="contact__border">
            <div class="contact__box">
                <div class="contact__col-1">
                    <form class="contact__form" method="post" name="contactform" action="mail.php">
                        <h3 class="contact__form-title">Send message</h3>
                    
                        <input type="text" placeholder="Name" name="name" id="name" required><br><br>
                    
                        <input type="email" placeholder="E-mail" name="email" id="email" required><br><br>
                    
                        <input type="text" placeholder="Phone number" name="phone" id="email" required><br><br>

                        <textarea class="contact__textarea" placeholder="Message" name="message" id="message" rows="4" required></textarea>

                        <input id="contact__button" class="contact__submit" type="submit" name="submit" value="Submit">
                    </form>

                    <p class="contact__form-rodo">The administrator of personal data GalacticStore Adam Gwiazdowicz,
                     Tarnowskie Góry, Konduktorska, nr 87, 42-600
                     NIP: 5751900847, REGON 386198135. 
                     The personal data provided will be used by the administrator only for contact purposes.
                    </p>

                </div>
                <div class="contact__col-2">
                    <div class="contact__head">
                        <div class="contact__paddle a-paddle"></div>
                        <h2 class="contact__title a-subtitle a-subtitle--white">Contact</h2>
                        <div class="contact__paddle a-paddle"></div>
                        <p class="contact__number a-number a-number--white">05</p>
                    </div>
                    <div class="contact__content">
                        <h3 class="contact__name a-data">Adam Gwiazdowicz</h3>
                        <h3 class="contact__adres a-data">gwiazdowiczadam@gmail.com</h3>
                        <a class="contact__phone a-data" href="tel:+48734468794">+48 734 468 794</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
</div> 



