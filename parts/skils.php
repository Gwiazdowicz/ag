<div class="section" id="section3">
    <div id="skils" class="skils">
        <div class="skils__container">
            <div class="skils__border">
                <div class="skils__box">
                    <div class="skils__head">
                        <div class="a-paddle"></div>
                        <h2 class="skils__title a-subtitle a-subtitle--white">Skils</h2>
                        <div class="a-paddle"></div>
                        <p class="skils__number a-number a-number--white">04</p>
                    </div>

                <div class="skils__controls">
                        <div id="arrow-left" class="skils__btn skils__btn--one"></div>
                        <div id="arrow-right" class="skils__btn skils__btn--two"></div>
                </div>

                        
                <div class="slick2">
                    <div>  
                        <div class="skils__about">
                            <div class="skils__text">
                                <h2 class="skils__discription a-discription a-subtitle--white">Programming</h2>
                                <p class="skils__article a-article a-article--white">Computer programmer skills often combine creativity with technical and analytical elements. I would like to present programmer technologies that I use in the software development process.</p>
                            </div>
                        </div>
    `
                        <div id="slick-1" class=" slick skils__slick">
                            
                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--one"></div>
                                    <p class="skils__name">Html</p>
                                </div>
                            </div>

                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--two"></div>
                                    <p class="skils__name">Css</p>
                                </div>
                            </div>

                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--three"></div>
                                    <p class="skils__name">Js</p>
                                </div>
                            </div>

                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--four"></div>
                                    <p class="skils__name">Jquery</p>
                                </div>
                            </div>

                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--eleven"></div>
                                    <p class="skils__name">C#</p>
                                </div>
                            </div>

                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--five"></div>
                                    <p class="skils__name">Wordpress</p>
                                </div>
                            </div>

                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--six"></div>
                                    <p class="skils__name">Php</p>
                                </div>
                            </div>

                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--seven"></div>
                                    <p class="skils__name">Ajax</p>
                                </div>
                            </div>
                            
                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--eight">B</div>
                                    <p class="skils__name">Bootstrap</p>
                                </div>
                            </div>

                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--nine"></div>
                                    <p class="skils__name">Shoper</p>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div>  
                        <div class="skils__about">
                            <div class="skils__text">
                                <h2 class="skils__discription a-discription a-subtitle--white">Graphic</h2>
                                <p class="skils__article a-article a-article--white">Creating a website always starts with working with a graphic design, knowledge of graphic programs support facilitates this process. Here are the graphics programs that I use.</p>
                            </div>
                        </div>
    `
                        <div class=" slick skils__slick">
                            
                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--one-g"></div>
                                    <p class="skils__name">Photoshop</p>
                                </div>
                            </div>

                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--two-g"></div>
                                    <p class="skils__name">Coreldraw</p>
                                </div>
                            </div>

                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--four-g"></div>
                                    <p class="skils__name">Figma</p>
                                </div>
                            </div>

                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--ten"></div>
                                    <p class="skils__name">Unity</p>
                                </div>
                            </div>

                            
                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--five-g"></div>
                                    <p class="skils__name">Gimp</p>
                                </div>
                            </div>

                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--three-g"></div>
                                    <p class="skils__name">Avocode</p>
                                </div>
                            </div>

                        </div> 
                    </div>

                    <div>  
                        <div class="skils__about">
                            <div class="skils__text">
                                <h2 class="skils__discription a-discription a-subtitle--white">Comunication</h2>
                                <p class="skils__article a-article a-article--white">I've worked with people for years. Which enabled me to develop communication skills. In the software development process, the following skills affect the quality of the software being created.</p>
                            </div>
                        </div>
    `
                        <div class=" slick skils__slick">
                            
                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--one-c"></div>
                                    <p class="skils__name">teamwork</p>
                                </div>
                            </div>

                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--two-c"></div>
                                    <p class="skils__name">Public speaking</p>
                                </div>
                            </div>

                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--three-c"></div>
                                    <p class="skils__name">
    interpersonal communication</p>
                                </div>
                            </div>

                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--five-c"></div>
                                    <p class="skils__name">Hard working</p>
                                </div>
                            </div>

                            <div class="skils__tile">
                                <div class="skils__logo">
                                    <div class="skils__image skils__image--four skils__image--four-c"></div>
                                    <p class="skils__name">team motivation</p>
                                </div>
                            </div>
                            
                        </div> 
                    </div>
                </div>    
            </div>
        </div>
    </div>
</div>
</div>
