<div class="section" id="section4">
<div id="realizations" class="realizations">
    <div class="realizations__container">
        <div class="realizations__border">

            <div class="skils__head">
                <div class="a-paddle"></div>
                <h2 class="skils__title a-subtitle a-subtitle--white">Projects</h2>
                <div class="a-paddle"></div>
                <p class="skils__number a-number a-number--white">05</p>
            </div>

            <div class="slick-r realizations__box">

                <a href="http://galacticshop.cba.pl/" class="realizations__tile">
                    <div class="realizations__image realizations__image--one"></div>
                </a>

                <a href="http://galacticshop-2.cba.pl/" class="realizations__tile">
                    <div class="realizations__image realizations__image--two"></div>
                </a>

                <a href="#!" class="realizations__tile">
                    <div class="realizations__image realizations__image--three"></div>
                </a>

                <a href="http://galacticshop-3.cba.pl/" class="realizations__tile">
                    <div class="realizations__image realizations__image--four"></div>
                </a>

                <a href="#!" class="realizations__tile">
                    <div class="realizations__image realizations__image--seven"></div>
                </a>

                <a href="http://galacticshop-4.cba.pl/" class="realizations__tile">
                    <div class="realizations__image realizations__image--five"></div>
                </a>

                <a href="https://sklep.egerton.pl/" class="realizations__tile">
                    <div class="realizations__image realizations__image--six"></div>
                </a>
            </div>
        </div>
    </div>
</div>
</div>