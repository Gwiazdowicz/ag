 <div class="hero">
 <div class="icon-bar">
        <a href="https://gitlab.com/Gwiazdowicz" class="lab"><i class="fa fa-gitlab"></i></a> 
        <a href="https://www.linkedin.com/in/adam-gwiazdowicz-749656194/" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="tel:+48-734-468-794" class="phone"><i class="fa fa-phone"></i></a> 
        <a href="mailto:gwiazdowiczadam@gmail.com" class="google"><i class="fa fa-google"></i></a>
    </div>


    <div class="hero__container">
        <div class="hero__border">
            <div class="hero__box">
                <div class="hero__star"></div>
                <div class="hero__icon-box">
                    <div class="hero__icon"></div>
                </div>
                <h1 class="hero__title a-title">Front-end developer</h1>
                <h2 class="hero__subtitle a-subtitle">Adam Gwiazdowicz</h2>
                <div class="hero__star hero__star--two"></div>
                <div class="hero__star hero__star--three"></div>
                <a href="#about-m" class="hero__chevron"></a>
            </div>
        </div>
    </div>
</div>




