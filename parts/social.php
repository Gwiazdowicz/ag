<div class="social">
    <div class="social__container">
        <div class="social__border">
            <div class="social__box">
                <div class="social__top-tile">
                    <div class="social__paddle a-paddle a-paddle--black"></div>
                    <h2 class="social__title a-subtitle">Welcome</h2>
                    <div class="social__paddle a-paddle a-paddle--black"></div>
                </div>
                <p class="social__number a-number">01</p>
            </div>
        </div>
    </div>
</div>
</div>
