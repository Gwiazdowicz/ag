<div class="section" id="section6">
<footer class="footer">
    <div class="footer__container">
        <div class="footer__border">
            <div class="footer__box">
                <div class="footer__col">
                    <ul>
                        <a href="#TopHero"><li>Menu</li></a>
                        <a href="#skil-m"><li>Skils</li></a>
                        <a href="#exp-m"><li>Experience</li></a>
                        <a href="#about-m"><li>About</li></a>
                        <a href="#cont-m"><li>Contact</li></a>
                    </ul>
                </div>
                <div class="footer__col-two">
                    <div class="footer__row">
                        <div class="footer__logo">
                            <a href="#TopHero"
                            >AG</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="#TopHero"><div class="footer__arrows"></div></a>
    <div class="footer__footer">
        <p>2020 © Made with passion by AG</p>
    </div>    
</footer> 
</div>
</div> 
</main>
</body>
</html>

